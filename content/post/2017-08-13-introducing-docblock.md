---
title: "Introducing DocBlock"
date: 2017-08-13
slug: "docblock"
---

## What?

DocBlock is a tool to define a documentation format for configuration files and to generate documentation from said configuration files.

Developers write comments in a pre-defined structure. Technical writers can then collect those comments in the configuration files to meaningful documentation by passing them through DocBlock.

## Why?

Have you ever ended up with a configuration file that doesn't have enough comments describing each element's function and thought "Gee I wish I had Java Doc style structure but for configuration files right now"?

No? Okay.

Well, this tool does just that. It's a structured way to comment on configuration files so that the comments can be collected in to a meaningful set of documentation.

By structuring and automating the parsing phase, DocBlock tries to bridge a gap between the developer and the technical writer when it comes to user-facing configuration files.

## How?

Normally a raw XML configuration file would look like the following.

```xml
<?xml version="1.0" encoding="utf-8" ?>
<root>
    <conf1>conf1</conf1>
    <conf2>
        <conf2.1>conf2.1</conf2.1>
        <conf2.2 attr="attribute">conf2.2</conf2.2>
        <!--<commentedConf>commented</commentedConf> -->
    </conf2>
</root>
```

With DocBlock you will be putting comments that describe the function of each configuration. For an example,

```xml
<?xml version="1.0" encoding="utf-8" ?>
<!-- @doc
Root configuration is the parent configuration for all options in this file.
-->
<root>
    <!-- @doc
    Conf1 controls functionality A

    @type string
    @possible_values conf1, conf2
    -->
    <conf1>conf1</conf1>
    <!-- @doc
    Conf2 contains configurations that control functionality B
    -->
    <conf2>
        <!-- @doc
        Conf 2.1 controls subfunctionality B.1

        @type boolean
        @possible_values true, false
        -->
        <conf2.1>false</conf2.1>
        <!-- @doc
        Conf 2.2 contains an attribute too

        @type string
        -->
        <conf2.2 attr="attribute">conf2.2</conf2.2>
        <!--<commentedConf>commented</commentedConf> -->
    </conf2>
</root>
```

When DocBlock operates on the above file, it would generate an output similar to the following.

```bash
docblock -file=sample-conf.xml -type=xml -out=markdown
```

-----
# sample-conf.xml


## 1. <code id="91947779410">&lt;root&gt;</code>

Root configuration is the parent configuration for all options in this file.

**Configurations**

| Element        | Attributes |  Description         | Type | Possible Values |
| :------------: |------------|----------------------|-------------|-------------|
| [`<conf1>`](#23082153551) |  -  | Conf1 controls functionality A | string | conf1, conf2 |
| [`<conf2>`](#11666145821) |  -  | Conf2 contains configurations that control functionality B |  -  |  -  |


### 1.1. <code id="23082153551">&lt;conf1&gt;</code>

Conf1 controls functionality A

### 1.2. <code id="11666145821">&lt;conf2&gt;</code>

Conf2 contains configurations that control functionality B

**Configurations**

| Element        | Attributes |  Description         | Type | Possible Values |
| :------------: |------------|----------------------|-------------|-------------|
| [`<conf2.1>`](#94235010051) |  -  | Conf 2.1 controls subfunctionality B.1 | boolean | true, false |
| [`<conf2.2>`](#16287113937) | attr<br /> | Conf 2.2 contains an attribute too | string |  -  |


#### 1.2.1. <code id="94235010051">&lt;conf2.1&gt;</code>

Conf 2.1 controls subfunctionality B.1


#### 1.2.2. <code id="16287113937">&lt;conf2.2&gt;</code>

Conf 2.2 contains an attribute too

-----

## TODO
1. **Immediate:** Add support for YAML configuration files (currently only supports XML files)
2. Parse comments in more meaningful ways (ex: skip license headers)
3. Add support for more output types (currently supports only Markdown and subsequent Markdown to HTML)

## License
DocBlock is licensed under Apache v2.0.
